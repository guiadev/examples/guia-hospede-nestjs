# Base image with node alpine version
FROM node:12.22.1-alpine3.10 as base

RUN apk add --no-cache bash

WORKDIR /home/app

# Production image
FROM base as production

COPY package*.json ./

RUN npm install --only=production

USER node

# Development image
FROM base as development

COPY package*.json ./

RUN npm install --only=development

USER node