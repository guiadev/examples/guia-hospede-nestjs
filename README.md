# guia-hospede-nestjs

Projeto de exemplo com serviço de Backend com API's da solução fictícia Guia Hóspede.

- Para visualizar o domínio utilizado como exemplo, acesse o [Modelo de domínio](https://guia.dev/pt/pillars/business/domain-model.html#desenho-modelo-dom%C3%ADnio).
- Este serviço de exemplo faz parte da solução Guia Hóspede, para entender mais sobre o negócio acesse o Modelo de negócio<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO>.
- As API's do serviço suportam as funcionalidades destinadas aos Hoteis e Hóspedes, para conhecer mais acesse o Mapeamento de Personas<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO> e Épicos e Histórias<<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO>>.

## Arquitetura e stack

Este serviço faz parte da solução fictícia Guia Hóspede, sendo ele um backend que contém todas as API's da solução.
Para conhecer mais sobre a arquitetura da solução Guia Hóspede, acesse o Desenho de arquitetura da solução<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO>.

O serviço está organizado em uma arquitetura de [camadas](https://guia.dev/pt/pillars/software-architecture/layers.html), para visualizar acesse o Desenho de arquitetura do projeto<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO>.

Detalhes da stack e integrações:
- A implementação é feita em [TypeScript](https://www.typescriptlang.org/) rodando sobre [Node 12](https://nodejs.org/).
- Uso do framework [NestJs 7.*](https://nestjs.com/).
- Persistência de dados é feita em um banco [PostgreSQL](https://www.postgresql.org/), acesse o modelo de dados<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO> para uma visualização resumida do modelo.
- As bibliotecas utilizadas pelo projeto podem ser vista nos arquivos que estão na raiz do projeto:
    - package.json

## Execução do projeto/sistema

O projeto está configurado para execução com [docker-compose](https://docs.docker.com/compose/), onde já estarão configuradas variáveis de ambiente e demais dependências.

Os scripts de execução estão implementados utilizando a ferramenta [npm](https://www.npmjs.com/)

#### **Na primeira vez que o projeto for executado**, execute:
```bash
$ docker-compose run --rm console npm run database:setup

# ou para subir banco e rodar localmente com npm
$ docker-compose up -d postgres
$ npm install
$ npm run database:setup
```

#### Para execução e testes:

```bash
# Para subir a aplicação
$ docker-compose up app

# ou para subir banco e rodar localmente com npm
$ docker-compose up -d postgres
$ npm run start
```

```bash
# Para executar os testes
$ docker-compose up test

# Ou
$ docker-compose up -d postgres
$ npm run test
```

```bash
# Para executar acesso ao console e rodar qualquer comando desejado
$ docker-compose run --rm console
```

Em ambiente local ou de testes, o sistema utiliza as variáveis de ambiente citadas abaixo, **já configuradas quando utilizado o docker-compose**.
- POSTGRES_HOST
- POSTGRES_PORT
- POSTGRES_DB
- POSTGRES_USER
- POSTGRES_PASSWORD

Em outros ambientes onde a aplicação for executada, basta informar a variável de ambiente:
- DATABASE_URL: com endereço completo de conexão com banco de dados Postgres.

---
## Alterações, teste e validação

Toda alteração no código deve ser realizada respeitando o processo de Git Flow<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO> definido.

---

O testes automatizados são executados através da ferramenta [jest](https://jestjs.io/), e podem ser executados conforme instruções na seção anterior.

Para validar e chamar as API's manualmente acesse a página com o [Swagger](https://swagger.io/):
- Local: http://localhost:3000/apidoc

As alterações deverão passar pelos Testes de componente/integração<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO> e Testes de Aceitação<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO>.

---

Para acesso aos ambientes onde o serviço está deployado, visite os ambientes de publicação<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO>
## Atualização e monitoramento.

Para enviar a Feature Branch para o ambiente desejado, deve-se seguir o processo de Merge request<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO>.

---

Para acesso a logs e monitoramento do ambiente de produção, leia Documentação DevOps<NÂO EXISTENTE PARA ESSE PROJETO DE EXEMPLO>
