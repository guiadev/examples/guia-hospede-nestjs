
## Focar no cliente! Priorize o que é mais crítico e utilizado.
* Ao olhar a execução dos testes, deve ser possível visualizar as funcionalidades, e não apenas "métodos testados"
* As funcionalidades mais críticas, são as que devem ter maior cobertura. Analisar relatórios de cobertura e testes.
* Para criar ou refatorar os testes, apartir dos relatórios montar uma lista ordenada priorizando as funcionalidades mais cíticas ao cliente, depois verificando o que está com menor cobertura, e terceiro quais testes estão mais complexos e/ou lentos.

## Automatizar os testes.
* ok! para os testes dos devs
* Homologação, será visto a automatização dos testes de aceitação.

## Maior cobertura com menor custo.
* Quantos testes há implementados para gerar a cobertura? O foco é menor complexidade nos testes implementados.

## Use o relatório de cobertura.
* Rodou o teste, analise a cobertura.
  * Rodar só um suite
    * Entender como o sistema se comporta, as dependencias e uso de outros módulos.
  * Rodar toda a suite
  * Rodar integrado
  * Analisar branches e condicionais não exercitadas

## Comece os testes pelas camadas lógicas mais altas.
* Não simplemente "testar o controller", mas sim testar a funcionalidade de um ponto que gere cobertura. O ponto mais alto depende de cada tipo de solução e necessidades

## Não dependa de terceiros para teste de componentes ou unitários.
* Cubra com testes até o ponto máximo da integração.
* Use a integração real no CI antes de ir para produção.
* De preferencia crie mecanismo para habilitar ou não os mocks

## Testes integrados validam integração entre componentes e terceiros, não negócio.
* Subir o aplicação o mais próximo possível de produção
* carregando todos os módulos
* testes focados nos itens relacionados a integração, como camadas de protocolo, integrações reais com terceiros

## Os testes e todos artefatos relacionados devem* estar agrupados e organizados.
* código da solução foca no business, não olha para coisas de testes
* pacote src não pode olhar para pacote test, não deve ter mocks, não deve ter nada referente ao cenário de testes
* pacote de testes foca em testar o código da solução, não tem lógica, apenas executa e valida os retornos.

## O código de testes deve ser legível, organizado e seguir bons princípios.
* Sempre construir blocos: setup/configuração, Cenário inicial/entrada, a chamada do que está sendo testado, resutlados esperados
* Uso de gwt
* beforeAll e afterAll e afapenas para setup da suite
* beforeEach e afterEach, deve ser usado com cautela, normalmente o item pertence a um cenário, o fato de repetir não faz ele ser válido para todos os testes da suite
* Testes não podem depender um dos outros
* use nomanclatura adequada, e não genérica
* Não invoque nos testes partes do sistema que não estão sendo chamadas
* Cenário de dados devem ser criados por fixtures, seeds, etc...


## Rodar o testes deve ser algo rápido.
* Não faça commit de dados, ao fazer isso é necessário apagar, use sempre rollback
* Carregue apenas o que é necessário para o teste em questão.
* Se algo é carregado sempre a cada teste, com os mesmos dados, busque alternativas para que isso seja carregado apenas uma vez.
* Crie/atualize o banco e popule com os cenários antes de iniciar os testes
* Use o volume mínimo de dados para garantir regras de negócio, volume de dados é para teste de carga/estress

