import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class ProfileCreate1621425052847 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "profile",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true
                },
                {
                    name: 'account_id',
                    type: 'int',
                    isNullable: false,
                    isUnique: true
                },
                {
                    name: "email",
                    type: "text",
                    isNullable: false
                },
                {
                    name: "phone",
                    type: "text",
                    isNullable: false
                },
                {
                    name: "timezone",
                    type: "text"
                },
                {
                    name: "website_url",
                    type: "text"
                },
                {
                    name: "created_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                },
                {
                    name: "updated_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                }
            ],
            foreignKeys: [
                {
                    columnNames: ['account_id'],
                    referencedTableName: 'account',
                    referencedColumnNames: ['id']
                },
            ],
        }), true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("profile");
    }

}
