import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class StayBookingCreate1621444955979 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "stay_booking",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true
                },
                {
                    name: "public_id",
                    type: "uuid",
                    isUnique: true,
                    isNullable: false
                },
                {
                    name: 'account_id',
                    type: 'int',
                    isNullable: false
                },
                {
                    name: "start_date",
                    type: "date",
                    isNullable: false
                },
                {
                    name: "end_date",
                    type: "date",
                    isNullable: false
                },
                {
                    name: "created_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                },
                {
                    name: "updated_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                }
            ],
            foreignKeys: [
                {
                    columnNames: ['account_id'],
                    referencedTableName: 'account',
                    referencedColumnNames: ['id']
                },
            ],
        }), true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("stay_booking");
    }

}
