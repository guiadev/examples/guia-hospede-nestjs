import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class AccommodationCreate1621445003390 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "accommodation",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true
                },
                {
                    name: "public_id",
                    type: "uuid",
                    isUnique: true,
                    isNullable: false
                },
                {
                    name: 'hotel_id',
                    type: 'int',
                    isNullable: false
                },
                {
                    name: 'description',
                    type: 'text',
                    isNullable: false
                },
                {
                    name: "created_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                },
                {
                    name: "updated_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                }
            ],
            foreignKeys: [
                {
                    columnNames: ['hotel_id'],
                    referencedTableName: 'hotel',
                    referencedColumnNames: ['id']
                },
            ],
        }), true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("accommodation");
    }

}
