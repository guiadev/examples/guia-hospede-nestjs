import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class AccountCreate1621097817423 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.createTable(new Table({
            name: "account",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true,
                },
                {
                    name: "public_id",
                    type: "uuid",
                    isUnique: true,
                    isNullable: false
                },
                {
                    name: "name",
                    type: "text",
                    isNullable: false,
                },
                {
                    name: "created_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                },
                {
                    name: "updated_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                }
            ]
        }), true)

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("account");
    }

}
