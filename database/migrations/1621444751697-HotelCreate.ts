import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class HotelCreate1621444751697 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "hotel",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true
                },
                {
                    name: "public_id",
                    type: "uuid",
                    isUnique: true,
                    isNullable: false
                },
                {
                    name: 'account_id',
                    type: 'int',
                    isNullable: false
                },
                {
                    name: "name",
                    type: "text",
                    isNullable: false
                },
                {
                    name: "zipcode",
                    type: "text",
                    isNullable: false
                },
                {
                    name: "address",
                    type: "text",
                    isNullable: false
                },
                {
                    name: "created_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                },
                {
                    name: "updated_at",
                    type: 'timestamp',
                    default: 'current_timestamp',
                    isNullable: false
                }
            ],
            foreignKeys: [
                {
                    columnNames: ['account_id'],
                    referencedTableName: 'account',
                    referencedColumnNames: ['id']
                },
            ],
        }), true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("hotel");
    }

}
