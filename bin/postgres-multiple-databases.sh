#!/bin/bash

set -e
set -u

function create_test_database() {
	local database="${POSTGRES_DB}_test"
	echo "  Creating user and database '$database'"
	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE USER $database;
	    CREATE DATABASE $database;
	    GRANT ALL PRIVILEGES ON DATABASE $database TO $database;
EOSQL
}

if [ -n "$POSTGRES_DB" ]; then
	create_test_database
fi