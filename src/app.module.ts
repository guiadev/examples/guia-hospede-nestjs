import { Module } from '@nestjs/common';
import { DomainModule } from './domain/domain.module';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IntegrationsModule } from './integrations/integrations.module';
import * as ormconfig from './ormconfig';
@Module({
  imports: [
    TypeOrmModule.forRoot(ormconfig),
    DomainModule,
    IntegrationsModule
  ],
  controllers: [AppController]
})
export class AppModule {}
