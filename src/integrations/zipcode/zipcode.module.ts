import { Module } from '@nestjs/common';
import { ZipCodeService } from './zipcode.service';

@Module({
  providers: [ZipCodeService],
  exports: [ZipCodeService]
})
export class ZipCodeModule {}
