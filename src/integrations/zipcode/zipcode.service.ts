import { Injectable } from '@nestjs/common';
import * as buscadorcep from 'buscadorcep';

@Injectable()
export class ZipCodeService {

    async getAddressByZipCode(cep: string): Promise<any> {
        const address = await buscadorcep(cep)
        return {
            street: address.logradouro,
            district: address.bairro,
            city: address.localidade,
            state: address.uf,
            zipCode: address.cep
        };
    }
}
