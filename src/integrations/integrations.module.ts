import { Module } from '@nestjs/common';
import { ZipCodeModule } from './zipcode/zipcode.module';

@Module({
  imports: [ZipCodeModule]
})
export class IntegrationsModule {}
