import { Module } from '@nestjs/common';
import { AccountService } from './account.service';
import { AccountController } from './account.controller';
import { Account } from './account.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Profile } from './profile/profile.entity';
import { ProfileService } from './profile/profile.service';

@Module({
  imports: [TypeOrmModule.forFeature([Account, Profile])],
  controllers: [AccountController],
  providers: [AccountService, ProfileService],
  exports: [AccountService]
})
export class AccountModule {}
