import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AccountDto } from './account.dto';
import { Account } from './account.entity';

@Injectable()
export class AccountService {

  constructor(
    @InjectRepository(Account) private readonly accountRepository: Repository<Account>,
  ) {}

  async create(accountDto: AccountDto): Promise<Account> {
    const account = this.accountRepository.create(accountDto);
    return this.accountRepository.save(account);
  }

  async findAll(): Promise<Account[]> {
    return this.accountRepository.find();
  }

  async findByPublicId(publicId: string): Promise<Account>  {
    return this.accountRepository.findOneOrFail({where: { publicId: publicId} });
  }

  async findIdByPublicId(publicId: string): Promise<number> {
    const account = await this.accountRepository.findOneOrFail({
      select: ["id"],
      where: { publicId: publicId}
    });
    return account.id;
  }

  async update(publicId: string, accountDto: AccountDto): Promise<Account> {
    const accountId = await this.findIdByPublicId(publicId);
    this.accountRepository.update(accountId , accountDto);
    return this.accountRepository.findOne(accountId);
  }

  async remove(publicId: string) {
    const accountId = await this.findIdByPublicId(publicId);
    return this.accountRepository.delete(accountId);
  }

}
