import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Account {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'public_id', nullable: false })
  publicId: string;

  @Column({nullable: false})
  name: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
