import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AccountService } from './account.service';
import { AccountDto } from './account.dto';
import { ProfileDto } from './profile/profile.dto';
import { ProfileService } from './profile/profile.service';

@Controller('accounts')
export class AccountController {
  constructor(
    private readonly accountService: AccountService,
    private readonly profileService: ProfileService
  ) {}

  @Post()
  create(@Body() AccountDto: AccountDto) {
    return this.accountService.create(AccountDto);
  }

  @Get()
  getAll() {
    return this.accountService.findAll();
  }

  @Get(':public_id')
  getByPublicId(@Param('public_id') publicId: string) {
    return this.accountService.findByPublicId(publicId);
  }

  @Patch(':public_id')
  update(@Param('public_id') publicId: string, @Body() accountDto: AccountDto) {
    return this.accountService.update(publicId, accountDto);
  }

  @Delete(':public_id')
  remove(@Param('public_id') publicId: string) {
    return this.accountService.remove(publicId);
  }

  @Get(':account_public_id/profile')
  getProfile(@Param('account_public_id') accountPublicId: string) {
    return this.profileService.find(accountPublicId);
  }

  @Patch(':account_public_id/profile')
  updateProfile(@Param('public_id') publicId: string, @Body() profileDto: ProfileDto) {
    return this.profileService.save(publicId, profileDto);
  }
}
