export class ProfileDto {
    email: string;
    phone: string;
    timezone: string;
    websiteUrl: string;
}
