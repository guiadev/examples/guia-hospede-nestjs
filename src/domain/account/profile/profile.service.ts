import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProfileDto } from './profile.dto';
import { Profile } from './profile.entity';
import { AccountService } from '../account.service';

@Injectable()
export class ProfileService {

  constructor(
    @InjectRepository(Profile) private readonly profileRepository: Repository<Profile>,
    private readonly accountService: AccountService
  ) {}

  async save(accountPublicId: string, profileDto: ProfileDto): Promise<Profile> {
    const accountId = await this.accountService.findIdByPublicId(accountPublicId);
    const profile = this.profileRepository.create(profileDto);
    profile.accountId = accountId;
    return this.profileRepository.save(profile);
  }

  async find(accountPublicId: string): Promise<Profile> {
    const accountId = await this.accountService.findIdByPublicId(accountPublicId);
    return this.profileRepository.findOneOrFail({where: { accountId: accountId} });
  }
}
