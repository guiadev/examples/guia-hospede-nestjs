import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Profile {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'account_id', nullable: false })
  accountId: number;

  @Column({nullable: false})
  email: string;

  @Column({nullable: false})
  phone: string;

  @Column({nullable: false})
  timezone: string;

  @Column({name: 'website_url', nullable: false})
  websiteUrl: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
