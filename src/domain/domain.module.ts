import { Module } from '@nestjs/common';
import { AccommodationModule } from './accommodation/accommodation.module';
import { AccountModule } from './account/account.module';
import { HotelModule } from './hotel/hotel.module';
import { StayBookingModule } from './stay-booking/stay-booking.module';


@Module({
  imports: [AccountModule, HotelModule, StayBookingModule, AccommodationModule]
})
export class DomainModule {}
