export class StayBookingDto {
    publicId: string;
    startDate: Date;
    endDate: Date;
    accountPublicId?: string;
}
