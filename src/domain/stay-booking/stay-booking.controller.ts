import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { StayBookingService } from './stay-booking.service';
import { StayBookingDto } from './stay-booking.dto';

@Controller('staybookings')
export class StayBookingController {
  constructor(private readonly stayBookingService: StayBookingService) {}

  @Post()
  create(@Body() StayBookingDto: StayBookingDto) {
    return this.stayBookingService.create(StayBookingDto);
  }

  @Get()
  getAll() {
    return this.stayBookingService.findAll();
  }

  @Get(':public_id')
  getByPublicId(@Param('public_id') publicId: string) {
    return this.stayBookingService.findByPublicId(publicId);
  }

  @Patch(':public_id')
  update(@Param('public_id') publicId: string, @Body() stayBookingDto: StayBookingDto) {
    return this.stayBookingService.update(publicId, stayBookingDto);
  }

  @Delete(':public_id')
  remove(@Param('public_id') publicId: string) {
    return this.stayBookingService.remove(publicId);
  }

}
