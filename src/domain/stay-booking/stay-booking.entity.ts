import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Account } from '../account/account.entity';

@Entity()
export class StayBooking {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'public_id', nullable: false })
  publicId: string;

  @ManyToOne(
    () => Account
  )
  @JoinColumn({ name: 'account_id' })
  account: Account;

  @Column({name: 'start_date', nullable: false})
  startDate: Date;

  @Column({name: 'end_date', nullable: false})
  endDate: Date;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
