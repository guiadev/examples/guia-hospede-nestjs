import { forwardRef, Module } from '@nestjs/common';
import { StayBookingService } from './stay-booking.service';
import { StayBookingController } from './stay-booking.controller';
import { StayBooking } from './stay-booking.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountService } from '../account/account.service';
import { Account } from '../account/account.entity';
import { AccountModule } from '../account/account.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([StayBooking]),
    forwardRef(() => AccountModule)
  ],
  controllers: [StayBookingController],
  providers: [StayBookingService]
})
export class StayBookingModule {}
