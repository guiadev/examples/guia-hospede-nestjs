import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AccountService } from '../account/account.service';
import { StayBookingDto } from './stay-booking.dto';
import { StayBooking } from './stay-booking.entity';

@Injectable()
export class StayBookingService {

  constructor(
    @InjectRepository(StayBooking) private readonly stayBookingRepository: Repository<StayBooking>,
    private readonly accountService: AccountService
  ) {}

  async create(stayBookingDto: StayBookingDto): Promise<StayBooking> {
    const stayBooking = this.stayBookingRepository.create(stayBookingDto);
    const account = await this.accountService.findByPublicId(stayBookingDto.accountPublicId);
    stayBooking.account = account;
    return this.stayBookingRepository.save(stayBooking);
  }

  async findAll(): Promise<StayBooking[]> {
    return this.stayBookingRepository.find();
  }

  async findByPublicId(publicId: string): Promise<StayBooking>  {
    return this.stayBookingRepository.findOneOrFail({where: { publicId: publicId} });
  }

  async findIdByPublicId(publicId: string): Promise<number> {
    const stayBooking = await this.stayBookingRepository.findOneOrFail({
      select: ["id"],
      where: { publicId: publicId}
    });
    return stayBooking.id;
  }

  async update(publicId: string, stayBookingDto: StayBookingDto): Promise<StayBooking> {
    const stayBookingId = await this.findIdByPublicId(publicId);
    this.stayBookingRepository.update(stayBookingId , stayBookingDto);
    return this.stayBookingRepository.findOne(stayBookingId);
  }

  async remove(publicId: string) {
    const stayBookingId = await this.findIdByPublicId(publicId);
    return this.stayBookingRepository.delete(stayBookingId);
  }

}
