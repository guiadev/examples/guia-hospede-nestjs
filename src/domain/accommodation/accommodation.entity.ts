import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Hotel } from '../hotel/hotel.entity';

@Entity()
export class Accommodation {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'public_id', nullable: false })
  publicId: string;

  @ManyToOne(
    () => Hotel
  )
  @JoinColumn({ name: 'hotel_id' })
  hotel: Hotel;

  @Column({nullable: false})
  description: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
