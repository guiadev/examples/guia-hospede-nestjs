import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { HotelService } from '../hotel/hotel.service';
import { AccommodationDto } from './accommodation.dto';
import { Accommodation } from './accommodation.entity';

@Injectable()
export class AccommodationService {

  constructor(
    @InjectRepository(Accommodation) private readonly accommodationRepository: Repository<Accommodation>,
    private readonly hotelService: HotelService
  ) {}

  async create(accommodationDto: AccommodationDto): Promise<Accommodation> {
    const accommodation = this.accommodationRepository.create(accommodationDto);
    const hotel = await this.hotelService.findByPublicId(accommodationDto.hotelPublicId);
    accommodation.hotel = hotel;
    return this.accommodationRepository.save(accommodation);
  }

  async findAll(): Promise<Accommodation[]> {
    return this.accommodationRepository.find();
  }

  async findByPublicId(publicId: string): Promise<Accommodation>  {
    return this.accommodationRepository.findOneOrFail({where: { publicId: publicId} });
  }

  async findIdByPublicId(publicId: string): Promise<number> {
    const accommodation = await this.accommodationRepository.findOneOrFail({
      select: ["id"],
      where: { publicId: publicId}
    });
    return accommodation.id;
  }

  async update(publicId: string, accommodationDto: AccommodationDto): Promise<Accommodation> {
    const accommodationId = await this.findIdByPublicId(publicId);
    this.accommodationRepository.update(accommodationId , accommodationDto);
    return this.accommodationRepository.findOne(accommodationId);
  }

  async remove(publicId: string) {
    const accommodationId = await this.findIdByPublicId(publicId);
    return this.accommodationRepository.delete(accommodationId);
  }

}
