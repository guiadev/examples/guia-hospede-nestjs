export class AccommodationDto {
    publicId: string;
    description: string;
    hotelPublicId?: string;
}
