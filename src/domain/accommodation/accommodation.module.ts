import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from '../account/account.entity';
import { AccountService } from '../account/account.service';
import { Hotel } from '../hotel/hotel.entity';
import { HotelModule } from '../hotel/hotel.module';
import { HotelService } from '../hotel/hotel.service';
import { AccommodationController } from './accommodation.controller';
import { Accommodation } from './accommodation.entity';
import { AccommodationService } from './accommodation.service';

@Module({
    imports: [
      TypeOrmModule.forFeature([Accommodation]),
      forwardRef(() => HotelModule)
    ],
    controllers: [AccommodationController],
    providers: [AccommodationService]
  })
export class AccommodationModule {}
