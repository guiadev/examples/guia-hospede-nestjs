import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AccommodationService } from './accommodation.service';
import { AccommodationDto } from './accommodation.dto';

@Controller('accommodations')
export class AccommodationController {
  constructor(private readonly accommodationService: AccommodationService) {}

  @Post()
  create(@Body() AccommodationDto: AccommodationDto) {
    return this.accommodationService.create(AccommodationDto);
  }

  @Get()
  getAll() {
    return this.accommodationService.findAll();
  }

  @Get(':public_id')
  getByPublicId(@Param('public_id') publicId: string) {
    return this.accommodationService.findByPublicId(publicId);
  }

  @Patch(':public_id')
  update(@Param('public_id') publicId: string, @Body() accommodationDto: AccommodationDto) {
    return this.accommodationService.update(publicId, accommodationDto);
  }

  @Delete(':public_id')
  remove(@Param('public_id') publicId: string) {
    return this.accommodationService.remove(publicId);
  }

}
