export class HotelDto {
    publicId: string;
    name: string;
    accountPublicId?: string;
    zipcode: string;
    address?: string;
}
