import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ZipCodeService } from '../../integrations/zipcode/zipcode.service';
import { Repository } from 'typeorm';
import { AccountService } from '../account/account.service';
import { HotelDto } from './hotel.dto';
import { Hotel } from './hotel.entity';

@Injectable()
export class HotelService {

  constructor(
    @InjectRepository(Hotel) private readonly hotelRepository: Repository<Hotel>,
    private readonly accountService: AccountService,
    private readonly zipCodeService: ZipCodeService
  ) {}

  async create(hotelDto: HotelDto): Promise<Hotel> {
    const hotel = this.hotelRepository.create(hotelDto);
    const account = await this.accountService.findByPublicId(hotelDto.accountPublicId);

    if (!hotelDto.address) {
      const address = await this.zipCodeService.getAddressByZipCode(hotelDto.zipcode);
      hotel.address = `${address.street}, ${address.district}, ${address.city} - ${address.state}`;
    }

    hotel.account = account;
    return this.hotelRepository.save(hotel);
  }

  async findAll(): Promise<Hotel[]> {
    return this.hotelRepository.find();
  }

  async findByPublicId(publicId: string): Promise<Hotel>  {
    return this.hotelRepository.findOneOrFail({where: { publicId: publicId} });
  }

  async findIdByPublicId(publicId: string): Promise<number> {
    const hotel = await this.hotelRepository.findOneOrFail({
      select: ["id"],
      where: { publicId: publicId}
    });
    return hotel.id;
  }

  async update(publicId: string, hotelDto: HotelDto): Promise<Hotel> {
    const hotelId = await this.findIdByPublicId(publicId);
    this.hotelRepository.update(hotelId , hotelDto);
    return this.hotelRepository.findOne(hotelId);
  }

  async remove(publicId: string) {
    const hotelId = await this.findIdByPublicId(publicId);
    return this.hotelRepository.delete(hotelId);
  }

}
