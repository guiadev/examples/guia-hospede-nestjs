import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Account } from '../account/account.entity';

@Entity()
export class Hotel {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'public_id', nullable: false })
  publicId: string;

  @ManyToOne(
    () => Account
  )
  @JoinColumn({ name: 'account_id' })
  account: Account;

  @Column({nullable: false})
  name: string;

  @Column({nullable: false})
  zipcode: string;

  @Column({nullable: false})
  address: string;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
