import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { HotelService } from './hotel.service';
import { HotelDto } from './hotel.dto';

@Controller('hotels')
export class HotelController {
  constructor(private readonly hotelService: HotelService) {}

  @Post()
  create(@Body() HotelDto: HotelDto) {
    return this.hotelService.create(HotelDto);
  }

  @Get()
  getAll() {
    return this.hotelService.findAll();
  }

  @Get(':public_id')
  getByPublicId(@Param('public_id') publicId: string) {
    return this.hotelService.findByPublicId(publicId);
  }

  @Patch(':public_id')
  update(@Param('public_id') publicId: string, @Body() hotelDto: HotelDto) {
    return this.hotelService.update(publicId, hotelDto);
  }

  @Delete(':public_id')
  remove(@Param('public_id') publicId: string) {
    return this.hotelService.remove(publicId);
  }

}
