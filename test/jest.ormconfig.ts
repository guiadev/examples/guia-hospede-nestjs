import {ConnectionOptions} from 'typeorm';
import * as ormconfig from '../src/ormconfig';

const config: ConnectionOptions = {
    ...ormconfig,
    type: 'postgres',
    database: `${process.env.POSTGRES_DB}_test`,
    logging: true,
    logger: 'file',
    migrations: ['database/migrations/**/*{.ts,.js}']
};

export = config;