import { Test, TestingModule } from "@nestjs/testing";
import { AppModule } from "../../src/app.module";

export class SingletonAppTestingModule {
    private static instance: SingletonAppTestingModule;
    private static app;

    private constructor() {}

    public static getInstance(): SingletonAppTestingModule {
        if (!SingletonAppTestingModule.instance) {
            SingletonAppTestingModule.instance = new SingletonAppTestingModule();
            console.log('Criei dinovo!!!');
        }

        return SingletonAppTestingModule.instance;
    }

    public async getApp() {
        if (!SingletonAppTestingModule.app){
            const moduleFixture: TestingModule = await Test.createTestingModule({
                imports: [AppModule],
            }).compile();
            SingletonAppTestingModule.app = moduleFixture.createNestApplication();
            await SingletonAppTestingModule.app.init();
        }
        return SingletonAppTestingModule.app;
    }
}