export default async () => {
    const default_address = {
        cep: "88010-000",
        logradouro: "Rua Felipe Schmidt",
        complemento: "",
        bairro: "Centro",
        localidade: "Florianópolis",
        uf: "SC",
        unidade: "",
        ibge: "",
        gia: ""
    }

    jest.mock('buscadorcep', () => {
        return jest.fn(() => default_address);
    });
}

