import mockBuscadorCep from './utils/mocks/buscadorcep';

if (!process.env.DISABLE_MOCKS){
    mockBuscadorCep();
}