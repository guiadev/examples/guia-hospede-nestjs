import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';
import {
  runInTransaction,
  initialiseTestTransactions,
} from 'typeorm-test-transactions';

initialiseTestTransactions();

describe('Account EndPoints', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  test('Get Accounts', async () => {
    const accounts = await request(app.getHttpServer())
      .get('/accounts')
      .expect(200);
  });

  test('Get Account by public id', async () => {
    const accountPublicId = "c22b48a8-c5a9-45e5-97d1-686d457b9f13";
    const accounts = await request(app.getHttpServer())
      .get(`/accounts/${accountPublicId}`)
      .expect(200);
  });

  test('Create Account', runInTransaction( async () => {
    const account = {
      name: "Account Nova",
      publicId: "5e4563ab-4d79-4c8d-93bf-fba661deedab"
    };

    const accounts = await request(app.getHttpServer())
      .post('/accounts')
      .send(account)
      .expect(201);
  }));

  test('Update Account', runInTransaction( async () => {
    const accountPublicId = "3a105bbd-8f95-4134-a744-62c8d1bf30c0";
    const account = {
      name: "Account updated",
      publicId: "a398dab7-c1bc-4db6-8b7e-306ffecabe8f"
    };

    const accounts = await request(app.getHttpServer())
      .patch(`/accounts/${accountPublicId}`)
      .send(account)
      .expect(200);
  }));

  test('Update Account', runInTransaction( async () => {
    const accountPublicId = "417f19c4-bea2-49f9-b3a5-434abd0a643f";

    const accounts = await request(app.getHttpServer())
      .delete(`/accounts/${accountPublicId}`)
      .expect(200);
  }));
});
