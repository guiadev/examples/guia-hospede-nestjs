import { StayBookingController } from '../../src/domain/stay-booking/stay-booking.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as ormconfig from '../jest.ormconfig';
import { StayBookingModule } from '../../src/domain/stay-booking/stay-booking.module';
import { StayBookingDto } from '../../src/domain/stay-booking/stay-booking.dto';
import {
  runInTransaction,
  initialiseTestTransactions,
} from 'typeorm-test-transactions';

initialiseTestTransactions();

describe('StayBooking Features', () => {
  let controller: StayBookingController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRoot(ormconfig), StayBookingModule]
    }).compile();

    controller = module.get<StayBookingController>(StayBookingController);
  });

  test('Get all stayBookings', async () => {
    const stayBookings = controller.getAll();

    await expect(stayBookings).resolves.toHaveLength(2);
    await expect(stayBookings).resolves.toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          publicId: '712f76fa-207e-45ed-9301-781065f4e0e3'
        }),
        expect.objectContaining({
          publicId: '3d8b349e-b29f-4d1c-ac14-e9507c38c1aa'
        }),
    ]));
  });

  test('Get stayBooking by public id', async () => {
    const publicId = "712f76fa-207e-45ed-9301-781065f4e0e3";

    const stayBooking = controller.getByPublicId(publicId);

    await expect(stayBooking).resolves.toEqual(
      expect.objectContaining({
        id: 1,
        publicId: '712f76fa-207e-45ed-9301-781065f4e0e3',
        startDate: expect.any(Date),
        endDate: expect.any(Date),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
      })
    );
  });

  test('Create StayBooking', runInTransaction(async () => {
    const stayBookingDto: StayBookingDto = {
      startDate: new Date(),
      endDate: new Date(),
      publicId: "2157d2a4-63fc-4bb0-becb-5b049b193a31",
      accountPublicId: "417f19c4-bea2-49f9-b3a5-434abd0a643f"
    };

    const stayBooking = controller.create(stayBookingDto);

    await expect(stayBooking).resolves.toMatchObject(
      expect.objectContaining({
        id: expect.any(Number),
        publicId: "2157d2a4-63fc-4bb0-becb-5b049b193a31",
        account: expect.objectContaining({ id: 4 }),
        startDate: expect.any(Date),
        endDate: expect.any(Date),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date)
      }));
  }));

  test('Update StayBooking', runInTransaction(async () => {
    const publicId = "3d8b349e-b29f-4d1c-ac14-e9507c38c1aa";
    const stayBookingDto: StayBookingDto = {
      startDate: new Date(),
      endDate: new Date(),
      publicId: "a398dab7-c1bc-4db6-8b7e-306ffecabe8f"
    };

    const updatedStayBooking = controller.update(publicId, stayBookingDto);

    await expect(updatedStayBooking).resolves.toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        publicId: "a398dab7-c1bc-4db6-8b7e-306ffecabe8f"
      }));
  }));

  test('Remove StayBooking', runInTransaction(async () => {
    const publicId = "3d8b349e-b29f-4d1c-ac14-e9507c38c1aa";

    const result = controller.remove(publicId);

    await expect(result).resolves.toMatchObject({affected: 1});
  }));
});