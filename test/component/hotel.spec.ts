import { HotelController } from '../../src/domain/hotel/hotel.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as ormconfig from '../jest.ormconfig';
import { HotelModule } from '../../src/domain/hotel/hotel.module';
import { HotelDto } from '../../src/domain/hotel/hotel.dto';
import {
  runInTransaction,
  initialiseTestTransactions,
} from 'typeorm-test-transactions';

initialiseTestTransactions();

describe('Hotel Features', () => {
  let controller: HotelController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRoot(ormconfig), HotelModule]
    }).compile();

    controller = module.get<HotelController>(HotelController);
  });

  test('Get all hotels', async () => {
    const hotels = controller.getAll();

    await expect(hotels).resolves.toHaveLength(10);
    await expect(hotels).resolves.toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Hotel 1',
          publicId: '712f76fa-207e-45ed-9301-781065f4e0e3'
        }),
        expect.objectContaining({
          name: 'Hotel 10',
          publicId: 'e4418637-1f61-4ca8-a8f2-b02d180c220b'
        }),
    ]));
  });

  test('Get hotel by public id', async () => {
    const publicId = "712f76fa-207e-45ed-9301-781065f4e0e3";

    const hotel = controller.getByPublicId(publicId);

    await expect(hotel).resolves.toEqual(
      expect.objectContaining({
        id: 1,
        name: 'Hotel 1',
        publicId: '712f76fa-207e-45ed-9301-781065f4e0e3',
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
      })
    );
  });

  test('Create Hotel without address', runInTransaction( async () => {
    const hotelDto: HotelDto = {
      name: "Hotel Novo",
      publicId: "2157d2a4-63fc-4bb0-becb-5b049b193a31",
      accountPublicId: "417f19c4-bea2-49f9-b3a5-434abd0a643f",
      zipcode: '88010000'
    };

    const hotel = controller.create(hotelDto);

    await expect(hotel).resolves.toMatchObject(
      expect.objectContaining({
        id: expect.any(Number),
        name: "Hotel Novo",
        publicId: "2157d2a4-63fc-4bb0-becb-5b049b193a31",
        account: expect.objectContaining({ id: 4 }),
        zipcode: '88010000',
        address: 'Rua Felipe Schmidt, Centro, Florianópolis - SC',
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date)
      }));
  }));

  test('Create Hotel with address', runInTransaction( async () => {
    const hotelDto: HotelDto = {
      name: "Hotel Novo",
      publicId: "2157d2a4-63fc-4bb0-becb-5b049b193a31",
      accountPublicId: "417f19c4-bea2-49f9-b3a5-434abd0a643f",
      zipcode: '88010000',
      address: 'Rua Felipe Schmidt, n 1002, Centro, Florianópolis - SC'
    };

    const hotel = controller.create(hotelDto);

    await expect(hotel).resolves.toMatchObject(
      expect.objectContaining({
        id: expect.any(Number),
        name: "Hotel Novo",
        publicId: "2157d2a4-63fc-4bb0-becb-5b049b193a31",
        account: expect.objectContaining({ id: 4 }),
        zipcode: '88010000',
        address: 'Rua Felipe Schmidt, n 1002, Centro, Florianópolis - SC',
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date)
      }));
  }));


  test('Update Hotel', runInTransaction(async () => {
    const publicId = "3d8b349e-b29f-4d1c-ac14-e9507c38c1aa";
    const hotelDto: HotelDto = {
      name: "Teste 2-updated",
      publicId: "a398dab7-c1bc-4db6-8b7e-306ffecabe8f",
      zipcode: '88010000'
    };

    const updatedHotel = controller.update(publicId, hotelDto);

    await expect(updatedHotel).resolves.toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        ...hotelDto
      }));
  }));

  test('Remove Hotel', runInTransaction(async () => {
    const publicId = "e4418637-1f61-4ca8-a8f2-b02d180c220b";

    const result = controller.remove(publicId);

    await expect(result).resolves.toMatchObject({affected: 1});
  }));
});