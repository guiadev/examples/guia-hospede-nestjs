import { AccountController } from '../../src/domain/account/account.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as ormconfig from '../jest.ormconfig';
import { AccountModule } from '../../src/domain/account/account.module';
import { AccountDto } from '../../src/domain/account/account.dto';
import { ProfileDto } from '../../src/domain/account/profile/profile.dto';
import {
  runInTransaction,
  initialiseTestTransactions,
} from 'typeorm-test-transactions';

initialiseTestTransactions();

describe('Account Features', () => {
  let controller: AccountController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRoot(ormconfig), AccountModule],
    }).compile();

    controller = module.get<AccountController>(AccountController);
  });

  test('Get all accounts', async () => {
    // Given

    // When
    const accounts = controller.getAll();

    //Then
    await expect(accounts).resolves.toHaveLength(5);
    await expect(accounts).resolves.toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Hotel Account 1',
          publicId: 'c22b48a8-c5a9-45e5-97d1-686d457b9f13'
        }),
        expect.objectContaining({
          name: 'Hotel Account 2',
          publicId: '3a105bbd-8f95-4134-a744-62c8d1bf30c0'
        }),
    ]));
  });

  describe('Get account by public id', () => {
    test('with existing uuid', async () => {
      // Given
      const publicId = "c22b48a8-c5a9-45e5-97d1-686d457b9f13";

      //When
      const account = controller.getByPublicId(publicId);

      //Then
      await expect(account).resolves.toEqual(
        expect.objectContaining({
          id: 1,
          name: 'Hotel Account 1',
          publicId: 'c22b48a8-c5a9-45e5-97d1-686d457b9f13',
          createdAt: expect.any(Date),
          updatedAt: expect.any(Date),
        })
      );
    });

    test('with non-existent uuid', async () => {
      // Given
      const publicId = "c22b48a8-c5a9-45e5-97d1-686d457b9f18";

      //When
      const account = controller.getByPublicId(publicId);

      //Then
      await expect(account).rejects.toThrow(
        expect.objectContaining({name: "EntityNotFound"})
      );
    });

    test('with invalid uuid', async () => {
      // Given
      const publicId = "11111111-1111-1111-1111-11111111111";

      //When
      const account = controller.getByPublicId(publicId);

      //Then
      await expect(account).rejects.toThrow(
        expect.objectContaining({
          name: "QueryFailedError",
          message: expect.stringContaining("invalid input syntax for type uuid")
        })
      );
    });
  });

  test('Create Account', runInTransaction( async () => {
    // Given
    const accountDto: AccountDto = {
      name: "Account Nova",
      publicId: "5e4563ab-4d79-4c8d-93bf-fba661deedab"
    };

    //When
    const account = controller.create(accountDto);

    //Then
    await expect(account).resolves.toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        ...accountDto
      }));
  }));

  test('Update Account', runInTransaction(async () => {
    // Given
    const publicId = "3a105bbd-8f95-4134-a744-62c8d1bf30c0";
    const accountDto: AccountDto = {
      name: "Account updated",
      publicId: "a398dab7-c1bc-4db6-8b7e-306ffecabe8f"
    };

    //When
    const updatedAccount = controller.update(publicId, accountDto);

    //Then
    await expect(updatedAccount).resolves.toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        ...accountDto
      }));
  }));

  test('Remove Account', runInTransaction(async () => {
    // Given
    const publicId = "417f19c4-bea2-49f9-b3a5-434abd0a643f";

    //When
    const result = controller.remove(publicId);

    //Then
    await expect(result).resolves.toMatchObject({affected: 1});
  }));

  test('Get Profile', () => {
    // Given
    const publicId = "c22b48a8-c5a9-45e5-97d1-686d457b9f13";

    //When
    const profile = controller.getProfile(publicId);

    //Then
    expect(profile).resolves.toEqual(
      expect.objectContaining({
        id: 1,
        accountId: 1,
        email: "email@account1.com",
        phone: "+55 48 1111 1111",
        timezone: "-3",
        websiteUrl: "www.account1.com",
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
      })
    );
  });

  test('Update Profile', runInTransaction(async () => {
    // Given
    const publicId = "3a105bbd-8f95-4134-a744-62c8d1bf30c0";
    const profileDto: ProfileDto = {
      email: "email@account2.com",
      phone: "+55 48 2222 2222",
      timezone: "-2",
      websiteUrl: "www.account2.com"
    };

    //When
    const updatedProfile = controller.updateProfile(publicId, profileDto);

    //Then
    await expect(updatedProfile).resolves.toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        accountId: 2,
        ...profileDto,
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date)
      }));
  }));
});