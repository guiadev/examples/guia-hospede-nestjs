import { AccommodationController } from '../../src/domain/accommodation/accommodation.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as ormconfig from '../jest.ormconfig';
import { AccommodationModule } from '../../src/domain/accommodation/accommodation.module';
import { AccommodationDto } from '../../src/domain/accommodation/accommodation.dto';
import {
  runInTransaction,
  initialiseTestTransactions,
} from 'typeorm-test-transactions';

initialiseTestTransactions();

describe('Accommodation Features', () => {
  let controller: AccommodationController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRoot(ormconfig), AccommodationModule]
    }).compile();

    controller = module.get<AccommodationController>(AccommodationController);
  });

  test('Get all accommodations', async () => {
    const accommodations = controller.getAll();

    await expect(accommodations).resolves.toHaveLength(15);
    await expect(accommodations).resolves.toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          description: 'Quarto duas camas solteiro',
          publicId: '712f76fa-207e-45ed-9301-781065f4e0e3'
        }),
        expect.objectContaining({
          description: 'Quarto simples solteiro',
          publicId: 'e4418637-1f61-4ca8-a8f2-b02d180c220b'
        }),
    ]));
  });

  test('Get accommodation by public id', async () => {
    const publicId = "712f76fa-207e-45ed-9301-781065f4e0e3";

    const accommodation = controller.getByPublicId(publicId);

    await expect(accommodation).resolves.toEqual(
      expect.objectContaining({
        id: 1,
        description: 'Quarto duas camas solteiro',
        publicId: '712f76fa-207e-45ed-9301-781065f4e0e3',
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
      })
    );
  });

  test('Create Accommodation', runInTransaction(async () => {
    const accommodationDto: AccommodationDto = {
      description: "Accommodation Novo",
      publicId: "2157d2a4-63fc-4bb0-becb-5b049b193a31",
      hotelPublicId: "e4418637-1f61-4ca8-a8f2-b02d180c220b"
    };

    const accommodation = controller.create(accommodationDto);

    await expect(accommodation).resolves.toMatchObject(
      expect.objectContaining({
        id: expect.any(Number),
        description: "Accommodation Novo",
        publicId: "2157d2a4-63fc-4bb0-becb-5b049b193a31",
        hotel: expect.objectContaining({ id: 10 }),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date)
      }));
  }));

  test('Update Accommodation', runInTransaction(async () => {
    const publicId = "712f76fa-207e-45ed-9301-781065f4e0e3";
    const accommodationDto: AccommodationDto = {
      description: "Teste 2-updated",
      publicId: "a398dab7-c1bc-4db6-8b7e-306ffecabe8f"
    };

    const updatedAccommodation = controller.update(publicId, accommodationDto);

    await expect(updatedAccommodation).resolves.toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        ...accommodationDto
      }));
  }));

  test('Remove Accommodation', runInTransaction(async () => {
    const publicId = "e4418637-1f61-4ca8-a8f2-b02d180c220b";

    const result = controller.remove(publicId);

    await expect(result).resolves.toMatchObject({affected: 1});
  }));
});